		<footer id="footer">
			<div class="footer1">
				<div class="inner clearfix">
					<div class="tel_btn">
						<a onclick="yahoo_report_conversion();goog_report_conversion('tel:XXX-XXX-XXX');" href="tel:03-6304-0971">
							お電話でのお問い合わせはこちらから
							TEL 03-6304-0971
						</a>
					</div>
					<div class="contact_btn">
						<a href="<?php echo get_page_link( 5 ); ?>">
							インターネットからはこちら
							お問い合わせ・資料請求
						</a>
					</div>
				</div>
			</div>
			<div class="to_top">
				<div class="inner">
					<a href="#">このページの先頭へ戻る</a>
				</div>
			</div>
			<div class="footer2">
				<div class="inner clearfix">
					<div class="logo"><a href="<?php echo home_url('/');?>">神社結婚式・神前式での和婚はBIKEN Bridal（</a></div>
					<div class="right">
						<nav class="fnav">
							<p class="title">神前式はBIKEN Bridal</p>
							<ul class="clearfix">
								<li><a href="<?php echo get_page_link( 2 ); ?>">BIKEN Bridalの神前式とは</a></li>
								<li><a href="<?php echo get_post_type_archive_link( 'location' ); ?>">神社一覧</a></li>
								<li><a href="<?php echo get_post_type_archive_link( 'plan' ); ?>">挙式プラン一覧</a></li>
								<li><a href="<?php echo get_page_link( 4 ); ?>#link_company">会社概要</a></li>
								<li><a href="<?php echo get_page_link( 5 ); ?>">お問い合わせ・資料請求</a></li>
							</ul>
						</nav>
						<div class="location_list_f">
							<p class="title"><a href="/location/">神社一覧</a></p>
							<ul class="clearfix">
								<?php $posts = get_posts('post_type=location&numberposts=20&order=asc'); foreach($posts as $post):?>
								<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
								<?php endforeach;?>
							</ul>
						</div>
						<div class="plan_list_f">
							<p class="title"><a href="/plan/">挙式プラン一覧</a></p>
							<ul class="clearfix">
								<?php $posts = get_posts('post_type=plan&numberposts=-1&order=asc'); foreach($posts as $post):?>
								<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
								<?php endforeach;?>
							</ul>
						</div>
						<p class="copyright">
							Copyright (C) 2014 BIKEN Bridal. All Rights Reserved.
						</p>
					</div>
				</div>
			</div>
		</footer>
		<!-- [/footer] -->

		<?php get_sidebar();?>

	</div>
	<!-- [/base] -->

<!-- [/Yahoo リスティング タグ] -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=Hjuu2Db";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=Hjuu2Db" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
</body>
<?php wp_footer();?>
</html>