<?php get_header(); ?>

	<?php if(in_category(1)):?>

		<div id="main_visual">
			<div class="inner">
				<h2 class="ph1">お知らせ</h2>
			</div>
		</div>
		<!-- [/main_visual] -->

		<div id="contents">
			<div class="inner">

				<p class="root_list"><?php if(function_exists('bcn_display')){bcn_display();} ?></p>

				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<section class="secc1 sec1">
					<h3 class="sh1"><span>お知らせ</span></h3>
					<div class="sb3">
						<p class="date"><?php echo date("Y.m.d", strtotime($post -> post_date));?></p>
						<h4 class="h4title"><?php the_title(); ?></h4>
						<div class="editor_cnt clearfix">
							<?php the_content(); ?>
						</div>
					</div>
				</section>
				<?php endwhile; ?>
				<?php endif;?>
				<!-- [/sec] -->

				<div class="banner_area_one">
					<a href="<?php echo get_post_type_archive_link( 'blogs' ); ?>">
						<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/blog.png" alt="挙式ブログ">
					</a>
				</div>
			</div>
		</div>
		<!-- [/contents] -->

	<?php elseif(in_category(2)):?>

		<div id="main_visual">
			<div class="inner">
				<h2 class="ph1">キャンペーン</h2>
			</div>
		</div>
		<!-- [/main_visual] -->

		<div id="contents">
			<div class="inner">

				<p class="root_list"><?php if(function_exists('bcn_display')){bcn_display();} ?></p>

				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<section class="secc1 sec2">
					<h3 class="sh1"><span>キャンペーン</span></h3>
					<div class="sb3">
						<p class="date"><?php echo date("Y.m.d", strtotime($post -> post_date));?></p>
						<h4 class="h4title"><?php the_title(); ?></h4>
						<div class="editor_cnt clearfix">
							<?php the_content(); ?>
						</div>
					</div>
				</section>
				<?php endwhile; ?>
				<?php endif;?>
				<!-- [/sec] -->

				<div class="banner_area_one">
					<a href="<?php echo get_post_type_archive_link( 'blogs' ); ?>">
						<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/blog.png" alt="挙式ブログ">
					</a>
				</div>
			</div>
		</div>
		<!-- [/contents] -->

	<?php endif;?>

<?php get_footer(); ?>
