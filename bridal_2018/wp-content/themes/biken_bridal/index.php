<?php get_header();?>

		<div id="contents">
			<div class="inner">

				<div class="index_banner_area">

					<div class="banner_area_one">
						<a href="<?php echo get_post_type_archive_link( 'blogs' ); ?>">
							<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/blog.png" alt="挙式ブログ">
						</a>
					</div>

					<ul class="banner_area_list">
<!--
						<li class="list_item">
							<a href="<?php echo home_url('/');?>campaign/472.html" target="_blank">
								<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/bnr_3jinjya.jpg" alt="神社３カ所巡りとプラン紹介ブライダルフェア">
							</a>
						</li>
-->
<!--
						<li class="list_item">
							<a href="<?php echo home_url('/');?>campaign/480.html" target="_blank">
								<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/bnr_plan.jpg" alt="プラン紹介とシェフ特製無料試食付きブライダルフェア">
							</a>
						</li>
 -->
						<li class="list_item">
							<a href="<?php echo home_url('/');?>campaign/481.html" target="_blank">
								<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/bnr_movie.jpg" alt="早割特典 挙式のムービープレゼント">
							</a>
						</li>
						<li class="list_item">
							<a href="<?php echo home_url('/');?>campaign/555.html" target="_blank">
								<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/bnr_asakusayushima.png" alt="浅草神社・湯島天神 平日期間限定プラン">
							</a>
						</li>
						<li class="list_item">
							<a href="<?php echo home_url('/');?>campaign/485.html" target="_blank">
								<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/bnr_jinrikisya.jpg" alt="新プラン登場 浅草神社×人力車 フォト神前式プラン">
							</a>
						</li>
						<li class="list_item">
							<a href="<?php echo home_url('/');?>campaign/482.html" target="_blank">
								<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/bnr_movie or cash.jpg" alt="夏季特典 ムービー撮影 or 7万円キャッシュバック">
							</a>
						</li>
<!-- 						<li class="list_item">
							<a href="<?php// echo home_url('/');?>campaign/483.html" target="_blank">
								<img class="trans_img80" src="<?php// bloginfo('template_directory'); ?>/img/banners/bnr_weekday 2person.jpg" alt="少人数 お二人だけの神前式 挙式のみプラン">
							</a>
						</li> -->
<!-- 						<li class="list_item">
							<a href="<?php// echo home_url('/');?>campaign/484.html" target="_blank">
								<img class="trans_img80" src="<?php// bloginfo('template_directory'); ?>/img/banners/bnr_weekday.jpg" alt="結婚式のみの基本プラン">
							</a>
						</li> -->
					</ul>
<!--
					<ul class="banner_area_list_single">
						<li class="list_item">
							<a href="campaign/443.html" target="_blank"><img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banner/bridalfair.jpg" alt="BIKEN Bridal ブライダルフェア 冬の特別相談会"></a>
						</li>
						<li class="list_item">
							<a href="campaign/441.html" target="_blank"><img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banner/plan.jpg" alt="BIKEN Bridal お得な2万円割引プラン"></a>
						</li>
					</ul>
-->
				</div>
				<!-- [/index_banner_area] -->

				<section class="secc1 sec1">
					<h3 class="sh1"><span>お知らせ</span></h3>
					<div class="sb1">
						<ul>
							<?php $posts = get_posts('numberposts=20&orderby=date&order=desc&post_type=post&category=1'); foreach($posts as $post):?>
							<li>
								<p class="date"><?php echo date("Y.m.d", strtotime($post -> post_date));?></p>
								<p class="data">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</p>
							</li>
							<?php endforeach;?>
						</ul>
						<p class="link">&gt;&gt;<a href="<?php echo get_category_link( 1 ); ?>">一覧を見る</a></p>
					</div>
				</section>
				<!-- [/sec] -->

				<section class="secc1 sec2">
					<h3 class="sh1"><span>キャンペーン情報</span></h3>
					<div class="sb1">
						<ul>
							<?php $posts = get_posts('numberposts=20&orderby=date&order=desc&post_type=post&category=2'); foreach($posts as $post):?>
							<li>
								<p class="date"><?php echo date("Y.m.d", strtotime($post -> post_date));?></p>
								<p class="data">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</p>
							</li>
							<?php endforeach;?>
						</ul>
						<p class="link">&gt;&gt;<a href="<?php echo get_category_link( 2 ); ?>">一覧を見る</a></p>
					</div>
				</section>
				<!-- [/sec] -->

				<section class="secc1 sec3">
					<h3 class="sh1"><span>神社一覧</span></h3>
					<div class="sb1 clearfix">
						<ul class="location_list clearfix">
							<?php query_posts('post_type=location&orderby=post_date&order=DESC&posts_per_page=12'); ?>
							<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>

							<!-- 神社1件分 start -->
							<li>
								<div class="img">
									<a href="<?php the_permalink(); ?>"><img class="trans_img80" src="<?php the_field("locat_main_img", $post->ID); ?>" alt="<?php the_title(); ?>" /></a>
								</div>
								<div class="text">
									<p class="name"><?php the_title(); ?></p>
									<p class="place"><?php the_field("locat_text1", $post->ID); ?></p>
								</div>
							</li>
							<!-- 神社1件分 end -->
							<?php endwhile; ?>
							<?php else:?>
							<?php endif;?>
							<?php wp_reset_query(); ?>
						</ul>

						<div class="to_list">
							<a class="trans_img80" href="<?php echo get_post_type_archive_link( 'location' ); ?>">全てを見る</a>
						</div>
					</div>
				</section>
				<!-- [/sec] -->

				<section class="secc1 sec4">
					<h3 class="sh1"><span>挙式プラン一覧</span></h3>
					<div class="sb1 clearfix">
						<ul class="plan_list clearfix">
							<?php query_posts('post_type=plan&orderby=post_date&order=DESC&posts_per_page=12'); ?>
							<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<?php $imagefield = get_field('plan_main_img'); ?>
								<?php $thum = wp_get_attachment_image_src($imagefield,'thumbnail');//var_dump($thum);?>
								<?php $check1 = get_field("plan_check1", $post->ID); ?>
								<?php $check2 = get_field("plan_check2", $post->ID); ?>
								<?php $check3 = get_field("plan_check3", $post->ID); ?>
								<?php $check4 = get_field("plan_check4", $post->ID); ?>
								<?php $check5 = get_field("plan_check5", $post->ID); ?>
								<?php $check6 = get_field("plan_check6", $post->ID); ?>
								<?php $textarea1 = get_field("plan_textarea1", $post->ID);?>
								<?php if(mb_strlen($textarea1)>84) {
									$textarea1 = mb_substr($textarea1,0,84).'..';}
									;?>
							<!-- 挙式プラン1件分 start -->
							<li>
								<p class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
								<div class="img_box clearfix">
									<div class="img">
										<a href="<?php the_permalink(); ?>"><img class="trans_img80" src="<?php echo $thum[0]; ?>" alt="<?php the_title(); ?>" /></a>
									</div>
									<div class="date">
										<p class="price"><?php the_field("plan_price1", $post->ID); ?></p>
										<p class="day"><?php the_field("plan_check1", $post->ID); ?></p>
										<ul class="marks">
											<?php if($check2):?>
											<li class="mark1">衣装付</li>
											<?php endif;?>
											<?php if($check3):?>
											<li class="mark2">美容付</li>
											<?php endif;?>
											<?php if($check4):?>
											<li class="mark3">写真付</li>
											<?php endif;?>
											<?php if($check5):?>
											<li class="mark4">持込無料</li>
											<?php endif;?>
											<?php if($check6):?>
											<li class="mark5">食事会付</li>
											<?php endif;?>
											<?php if($check1 == "土日可"):?>
											<li class="mark6">土日限定</li>
											<?php endif;?>
										</ul>
									</div>
								</div>
								<p class="text">
									<?php echo $textarea1; ?>
								</p>
							</li>
							<!-- 挙式プラン1件分 end -->
							<?php endwhile; ?>
							<?php else:?>
							<?php endif;?>
							<?php wp_reset_query(); ?>
						</ul>

						<div class="to_list">
							<a class="trans_img80" href="<?php echo get_post_type_archive_link( 'plan' ); ?>">全てを見る</a>
						</div>
					</div>
				</section>
				<!-- [/sec] -->

				<section class="secc1 sec5">
					<h3 class="sh1"><span>挙式風景</span></h3>
					<div class="sb1 clearfix">
						<ul class="photo_list clearfix">
							<?php query_posts('post_type=top_photo&orderby=post_date&order=DESC&posts_per_page=-1'); ?>
							<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
								<?php $imagefield = get_field('top_photo_img1'); ?>
								<?php $image = wp_get_attachment_image_src($imagefield,'full');//var_dump($thum);?>
								<?php $thum = wp_get_attachment_image_src($imagefield,'midium');//var_dump($thum);?>
							<!-- 写真1件分 start -->
							<li>
								<a href="<?php echo $image[0]; ?>">
									<img class="photo trans_img80" src="<?php echo $thum[0]; ?>" alt="<?php the_title(); ?>" />
									<img class="mark" src="<?php bloginfo('template_directory'); ?>/img/common/mark1.png" alt="" />
								</a>
							</li>
							<!-- 写真1件分 end -->
							<?php endwhile; ?>
							<?php else:?>
							<?php endif;?>
							<?php wp_reset_query(); ?>
						</ul>
					</div>
				</section>
				<!-- [/sec] -->

				<div class="bnrs clearfix">
					<div class="bnr1">
						<a href="<?php echo get_page_link( 2 ); ?>#link_flow"><img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/index/btn3.png" alt="結婚式までの流れ" /></a>
					</div>
					<div class="bnr2">
						<a href="<?php echo get_page_link( 4 ); ?>"><img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/index/btn4.png" alt="ご来店案内" /></a>
					</div>
				</div>

				<div class="banner_area_one skin_foot">
					<a href="<?php echo get_post_type_archive_link( 'blogs' ); ?>">
						<img class="trans_img80" src="<?php bloginfo('template_directory'); ?>/img/banners/blog.png" alt="挙式ブログ">
					</a>
				</div>

			</div>
		</div>
		<!-- [/contents] -->

<?php get_footer();?>