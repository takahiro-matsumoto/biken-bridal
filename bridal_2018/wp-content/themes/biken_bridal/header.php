<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<meta name="format-detection" content="telephone=no">
<?php if(get_post_type()==='venue' or is_post_type_archive('venue')):?>
	<meta name="robots" content="noindex,nofollow">
<?php else:?>
	<meta name="robots" content="noodp,noydir" />
<?php endif;?>
<?php if(is_home() or is_404()):?>
	<meta name="description" content="格安 神社結婚式ならBIKEN Bridalへお任せください。格安でご満足いただける神前式・和婚をお手伝いします。">
	<meta name="keywords" content="神前式,神社結婚式,和婚,格安">
<?php else:?>
	<meta name="description" content="<?php wp_title( '。', true, 'right' );?>神社結婚式ならBIKEN Bridalへお任せください。格安でご満足いただける神前式・和婚をお手伝いします。">
	<meta name="keywords" content="<?php wp_title( '', true, 'right' );?>,神前式,神社結婚式,和婚,格安">
<?php endif;?>
<?php if(is_home()):?>
	<title>神社結婚式・神前式での和婚はBIKEN Bridal（美研ブライダル）</title>
<?php elseif(get_post_type()==='location'): ?>
	<title>神前式・神社結婚式 <?php wp_title( ' | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(is_post_type_archive('location')): ?>
	<title>神前式・神社結婚式 <?php wp_title( 'の一覧 | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(get_post_type()==='plan'): ?>
	<title>神前式・神社結婚式 <?php wp_title( ' | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(is_post_type_archive('plan')): ?>
	<title>神前式・神社結婚式 <?php wp_title( 'の一覧 | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(get_post_type()==='venue'): ?>
	<title>神前式・神社結婚式 <?php wp_title( ' | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(is_post_type_archive('venue')): ?>
	<title>神前式・神社結婚式 <?php wp_title( 'の一覧 | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(is_single()):?>
	<title><?php wp_title( ' | ', true, 'right' );bloginfo( 'name' );?></title>
<?php elseif(is_category()):?>
	<title><?php wp_title( 'の一覧 | ', true, 'right' );bloginfo( 'name' );?></title>
<?php else:?>
<title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
	?></title>
<?php endif;?>
	<link rel="index" href="<?php echo home_url('/');?>" title="格安の神社結婚式・神前式での和婚はBIKEN Bridal">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen and (min-width: 641px)" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style_sp.css" media="screen and (max-width: 640px)" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/parts.css" />
	<?php if(is_page(5)):?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/mfp.statics/mailformpro.css" type="text/css" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/mfp.statics/thanks.js"></script>
	<?php elseif(is_page(506)):?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/mfp.statics/mailformpro.css" type="text/css" />
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/mfp.statics/thanks.js"></script>
	<?php endif;?>
	<!-- [/CSS] -->
	<?php get_template_part('template_head_js'); ?>
	<?php wp_head();?>
	<?php get_template_part('template_analytics'); ?>
</head>
<?php get_template_part('template_body'); ?>
<div id="base">

		<header id="header">
			<div class="inner">
				<div class="logo"><a href="<?php echo home_url('/');?>" title="格安 神社結婚式（神前式） BIKEN Bridal">格安 神社結婚式（神前式） BIKEN Bridal</a></div>
				<h1><?php get_template_part('template_head_h1'); ?></h1>
				<p class="header_text">
					<strong>79,800円からの、本格的な神社結婚式なら。</strong><br />
					BIKENは衣裳・美容・写真も全て含めてご提供します。
				</p>
				<div class="tel1">03-6304-0971</div>
				<div class="tel2"><a onclick="yahoo_report_conversion();goog_report_conversion('tel:XXX-XXX-XXX');" href="tel:03-6304-0971">03-6304-0971</a></div>
				<div class="contact_btn">
					<a href="<?php echo get_page_link( 5 ); ?>"><img class="trans_img" src="<?php bloginfo('template_directory'); ?>/img/common/head_bt_contact1.png" alt="お問い合わせ・資料請求" /></a>
				</div>
				<div class="access_btn">
					<a href="<?php echo get_page_link( 4 ); ?>"><img class="trans_img" src="<?php bloginfo('template_directory'); ?>/img/common/head_btn_access.png" alt="会社概要・アクセス" /></a>
				</div>
<!--
				<nav class="hnav">
					<ul class="clearfix">
						<li><a href="<?php// echo get_category_link( 1 ); ?>">お知らせ</a></li>
						<li><a href="<?php// echo get_page_link( 4 ); ?>">アクセス</a></li>
					</ul>
				</nav>
 -->
			</div>
		</header>
		<!-- [/header] -->

		<?php get_template_part('template_gnav'); ?>

		<?php if(is_home() or is_404()):?>
		<div id="main_visual">
			<div class="inner">
				<div class="text_img1">
					和婚・神前式はBIKEN
				</div>
				<div class="text_img2">
					79,800円＋初穂料で神社結婚式。
					親族だけの少人数でもOK。
					料金を心配しなくていい神前式なら。
				</div>
				<div class="text_img3">
					BIKEN Bridalの神前式は、
					衣装代・美容代・写真代が全て含まれています。
				</div>
				<?php $posts = get_posts('numberposts=1&orderby=date&order=desc&post_type=top_plan'); foreach($posts as $post):?>
				<div class="text_img4">
					<div class="bg_btm">
						<div class="bg_mid">
							<p class="text1">
								挙式プラン <span>「<a href="<?php the_field("top_plan_text3");?>"><?php the_field("top_plan_text1");?></a>」</span><br />
								<?php// the_field("top_plan_text2");?><b class="fs24 fw_b">79,800円</b> <em class="fs10">(税別)</em>（土日祝＋30,000円）+初穂料
							</p>
							<p class="text2">
								<?php the_field("top_plan_textarea1");?>
							</p>
						</div>
					</div>
				</div>
				<?php endforeach;?>
			</div>
		</div>
		<!-- [/main_visual] -->

		<div class="to_plan">
			<div class="inner clearfix">
				<div class="text_img1">
					BIKEN Bridalのご提供するプランは、
					衣装・美容・写真代が全て料金に含まれています。
				</div>
				<div class="text_img2">
					《会場持込み可能でお得》
					BIKEN Bridalの衣装・美容・写真なら、
					提携会場への持込料がかかりません。
				</div>
				<div class="text_img3">
					《伝統ある神社で厳かな本格神前式》
					熊野神社を始めとした、由緒正しい神社での
					神前式をご提供しています。
				</div>
				<div class="detail_btn">
					<a class="trans_img80" href="<?php echo get_page_link( 2 ); ?>">詳しくはこちら</a>
				</div>
			</div>
		</div>
		<?php endif;?>
