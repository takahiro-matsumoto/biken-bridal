<nav id="fixed_nav">
	<div class="bg_top">
		<div class="bg_btm">
			<ul>
				<li class="nav01"><a href="<?php echo home_url('/');?>">TOP</a></li>
				<li class="nav02"><a href="<?php echo get_post_type_archive_link( 'location' ); ?>">神社一覧</a></li>
				<li class="nav03"><a href="<?php echo get_post_type_archive_link( 'plan' ); ?>">プラン一覧</a></li>
				<!-- <li class="nav04"><a href="<?php// echo get_post_type_archive_link( 'venue' ); ?>">会場一覧</a></li> -->
				<li class="nav05"><a href="<?php echo get_page_link( 506 ); ?>">フェア予約</a></li>
				<li class="nav06"><a href="<?php echo get_page_link( 5 ); ?>">資料請求</a></li>
				<li class="nav07"><a href="<?php echo get_page_link( 4 ); ?>">アクセス</a></li>
			</ul>
		</div>
	</div>
</nav>
